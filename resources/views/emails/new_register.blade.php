<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
    </head>
    <body class="antialiased">
        <div class="justify-center">
            <ul>
                <li>Login: {{$user->name}}</li>
                <li>Email: {{$user->email}}</li>
            </ul>
        </div>
    </body>
</html>
