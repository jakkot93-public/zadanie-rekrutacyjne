<?php

return [
    'status.ok' => 'success',
    'status.error' => 'error',

    'error_message.invalid_login' => 'invalid login details',
];
