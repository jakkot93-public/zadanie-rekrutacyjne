<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

# Zadanie rekrutacyjne

[![GitLab](https://img.shields.io/badge/GitLab-%40jakkot93-green)](https://gitlab.com/jakkot93-public/multi-tenant-api)
![GitLab tag (latest by date)](https://img.shields.io/gitlab/v/tag/jakkot93-public/multi-tenant-api?color=blue&logo=Gitlab&sort=date&)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

### Wymagania

- [Composer](https://getcomposer.org/download/)
- [Docker](https://docs.docker.com/get-docker/)

---
## Użyte bilbioteki

| Biblioteka | Wersja | Licencja                                                                                                      | Repozytorium |
| ---------- | ------ |---------------------------------------------------------------------------------------------------------------| ------------ |
| PHP        | ![PHP Version](https://img.shields.io/badge/PHP-v8.1-blue) | [![Licencja: PHP](https://img.shields.io/badge/Licencja-PHP-blue.svg)](https://www.php.net/license/)          | [php/php-src](https://github.com/php/php-src) |
| Laravel Framework | ![Laravel Version](https://img.shields.io/badge/Laravel-v10.0-orange) | [![Licencja: MIT](https://img.shields.io/badge/Licencja-MIT-yellow.svg)](https://opensource.org/licenses/MIT) | [laravel/framework](https://github.com/laravel/framework) |
| Laravel Sanctum | ![Sanctum Version](https://img.shields.io/badge/Laravel_Sanctum-v3.2-green) | [![Licencja: MIT](https://img.shields.io/badge/Licencja-MIT-yellow.svg)](https://opensource.org/licenses/MIT) | [laravel/sanctum](https://github.com/laravel/sanctum) |
| Laravel UUID | ![UUID Version](https://img.shields.io/badge/Laravel_UUID-v4.0-purple) | [![Licencja: MIT](https://img.shields.io/badge/Licencja-MIT-yellow.svg)](https://opensource.org/licenses/MIT) | [webpatser/laravel-uuid](https://github.com/webpatser/laravel-uuid) |
| Laravel Sail | ![Sail Version](https://img.shields.io/badge/Laravel_Sail-v1.21-blue) | [![Licencja: MIT](https://img.shields.io/badge/Licencja-MIT-yellow.svg)](https://opensource.org/licenses/MIT) | [laravel/sail](https://github.com/laravel/sail) |
| L5 Swagger | ![L5 Swagger Version](https://img.shields.io/badge/L5_Swagger-v8.5-green) | [![Licencja: MIT](https://img.shields.io/badge/Licencja-MIT-yellow.svg)](https://opensource.org/licenses/MIT) | [darkaonline/l5-swagger](https://github.com/DarkaOnLine/L5-Swagger) |

---
## Uruchomienie

```bash
docker run --rm --interactive --tty --volume $PWD:/app composer install

cp .env.example .env

./vendor/bin/sail up

./vendor/bin/sail artisan key:generate
./vendor/bin/sail artisan migrate

./vendor/bin/sail artisan queue:work --queue=images
```

---
## Dokumentacja

[http://localhost/api/documentation](http://localhost/api/documentation)
