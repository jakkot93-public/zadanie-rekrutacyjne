<?php

use App\Domains\Image\Http\Controllers\ImageController;
use App\Http\Controllers\Api\Auth\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/login', [AuthController::class, 'login']);
});

Route::middleware('auth:sanctum')->group(function () {

    // image
    Route::group(
        ['prefix' => 'images'],
        function () {
            Route::get('/', [ImageController::class, 'index']);
            Route::post('/', [ImageController::class, 'store']);
            Route::get('/{image}', [ImageController::class, 'show']);
            Route::delete('/{image}', [ImageController::class, 'destroy']);
        }
    );
});
