<?php

namespace App\ModelTraits;

use Webpatser\Uuid\Uuid;

trait Uuids
{
    /**
     * Boot function from laravel.
     */
    protected static function bootUuids()
    {
        static::creating(function ($model) {
            if (!$model->uuid) {
                $model->uuid = Uuid::generate()->string;
            }
        });
    }

}
