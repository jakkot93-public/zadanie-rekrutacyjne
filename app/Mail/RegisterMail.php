<?php

namespace App\Mail;

use App\Models\Eloquent\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegisterMail extends Mailable
{
    use Queueable, SerializesModels;

    const SUBJECT_TRANSLATION_KEY = 'mail.new_register';
    const VIEW_NAME = 'emails.new_register';

    /**
     * @var User
     */
    public $user;

    public function setUser(User $user): RegisterMail
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(__(self::SUBJECT_TRANSLATION_KEY))
            ->view(self::VIEW_NAME);
    }
}
