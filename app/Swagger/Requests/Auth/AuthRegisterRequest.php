<?php

namespace App\Swagger\Requests\Auth;

/**
 * @OA\Schema(
 *      title="Auth Register Request",
 *      description="Auth register",
 *      type="object",
 *      required={"login","email","password","password_confirmation"}
 * )
 */
class AuthRegisterRequest
{
    /**
     * @OA\Property(
     *  title="login",
     *  description="login",
     *  example="login"
     * )
     *
     * @var string
     */
    public $login;

    /**
     * @OA\Property(
     *  title="email",
     *  description="email",
     *  example="jakub@jakub.pl"
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *  title="password",
     *  description="password",
     *  example="password"
     * )
     *
     * @var string
     */
    public $password;

    /**
     * @OA\Property(
     *  title="password_confirmation",
     *  description="password_confirmation",
     *  example="password"
     * )
     *
     * @var string
     */
    public $password_confirmation;
}
