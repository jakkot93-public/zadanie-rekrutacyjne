<?php

namespace App\Swagger\Requests\Auth;

/**
 * @OA\Schema(
 *      title="Login Request",
 *      description="Login",
 *      type="object",
 *      required={"email","password"}
 * )
 */
class LoginRequest
{
    /**
     * @OA\Property(
     *  title="email",
     *  description="email",
     *  example="jakub@jakub.pl"
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *  title="password",
     *  description="password",
     *  example="password"
     * )
     *
     * @var string
     */
    public $password;
}
