<?php

namespace App\Models;

/**
 * Interface UserInterfaces
 */
interface UserInterfaces
{
    const TABLE_NAME = 'users';

    const ATTRIBUTE_ID = 'id';
    const ATTRIBUTE_NAME = 'name';
    const ATTRIBUTE_EMAIL = 'email';

    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail(string $email): self;

    /**
     * @return string
     */
    public function getEmail(): string;

}
