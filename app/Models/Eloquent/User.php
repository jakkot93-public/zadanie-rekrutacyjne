<?php

namespace App\Models\Eloquent;

use App\Domains\Image\Models\Eloquent\Image;
use App\Domains\Image\Models\ImageInterfaces;
use App\Events\UserCreating;
use App\Models\UserInterfaces;
use App\ModelTraits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements UserInterfaces
{
    use HasApiTokens, HasFactory, Notifiable, Uuids;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = UserInterfaces::TABLE_NAME;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @var array
     */
    protected $dispatchesEvents = [
        'creating' => UserCreating::class,
    ];

    /**
     * @inheritDoc
     */
    public function getId(): int
    {
        return $this->getAttribute(UserInterfaces::ATTRIBUTE_ID);
    }

    /**
     * @inheritDoc
     */
    public function setName(string $name): UserInterfaces
    {
        return $this->setAttribute(UserInterfaces::ATTRIBUTE_NAME, $name);
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return $this->getAttribute(UserInterfaces::ATTRIBUTE_NAME);
    }

    /**
     * @inheritDoc
     */
    public function setEmail(string $email): UserInterfaces
    {
        return $this->setAttribute(UserInterfaces::ATTRIBUTE_EMAIL, $email);
    }

    /**
     * @inheritDoc
     */
    public function getEmail(): string
    {
        return $this->getAttribute(UserInterfaces::ATTRIBUTE_EMAIL);
    }

    /**
     * @return HasMany
     */
    public function images(): HasMany
    {
        return $this->hasMany(
            Image::class,
            ImageInterfaces::ATTRIBUTE_USER_ID,
            self::ATTRIBUTE_ID
        );
    }
}
