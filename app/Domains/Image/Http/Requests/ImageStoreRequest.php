<?php

namespace App\Domains\Image\Http\Requests;

use App\Domains\Image\Models\ImageInterfaces;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class ImageStoreRequest extends FormRequest
{

    public function rules()
    {
        $rules = [
            'image' => 'required|image|mimes:jpeg,png,jpg|max:20480',
            'settings.type' => ['required', 'string', Rule::in(ImageInterfaces::SETTINGS_TYPES)],
        ];

        $settings = request()->get('settings');
        $type = Arr::get($settings, 'type');

        if ($type === ImageInterfaces::SETTINGS_TYPE_SCALING) {
            $rules['settings.value'] = 'nullable|integer|between:0,100';
        } else {
            $rules['settings.value'] = 'required|in:jpeg,png,jpg';
        }

        return $rules;
    }
}
