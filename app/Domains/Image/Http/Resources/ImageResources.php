<?php

namespace App\Domains\Image\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ImageResources extends JsonResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'settings' => $this->settings,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
