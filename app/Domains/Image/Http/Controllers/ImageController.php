<?php

namespace App\Domains\Image\Http\Controllers;

use App\Domains\Image\Http\Requests\ImageStoreRequest;
use App\Domains\Image\Http\Resources\ImageResources;
use App\Domains\Image\Models\Eloquent\Image;
use App\Domains\Image\Services\ImageService;
use App\Http\Controllers\Api\AbstractApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class ImageController extends AbstractApiController
{

    /**
     * @param ImageService $imageService
     */
    public function __construct(
        private ImageService $imageService
    )
    {
        //
    }

    /**
     * @param Request $request
     * @return AnonymousResourceCollection
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        $images = $this->imageService->list($request->user());

        return ImageResources::collection(
            $images->paginate($request->get('per_page', self::PER_PAGE_DEFAULT))
        );
    }

    /**
     * @param ImageStoreRequest $request
     * @return ImageResources
     */
    public function store(ImageStoreRequest $request): ImageResources
    {
        Log::channel('images')->info([
            'user_id' => $request->user()->id,
            'action' => 'create',
            'request' => $request->validated()
        ]);

        return new ImageResources(
            $this->imageService->create($request, $request->user())
        );
    }

    /**
     * @param Request $request
     * @param Image $image
     * @return ImageResources
     */
    public function show(Request $request, Image $image): ImageResources
    {
        abort_if($request->user()->cannot('view', $image), ResponseAlias::HTTP_FORBIDDEN);

        return new ImageResources($image);
    }

    /**
     * @param Request $request
     * @param Image $image
     * @return JsonResponse
     */
    public function destroy(Request $request, Image $image): JsonResponse
    {
        abort_if($request->user()->cannot('view', $image), ResponseAlias::HTTP_FORBIDDEN);

        $image->delete();

        return $this->response(['status' => 'ok']);
    }
}
