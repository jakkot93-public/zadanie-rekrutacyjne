<?php

namespace App\Domains\Image\Repository\Eloquent;

use App\Domains\Image\Models\Eloquent\Image;
use App\Domains\Image\Models\ImageInterfaces;
use App\Domains\Image\Repository\ImageRepositoryInterface;
use App\Models\Eloquent\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

class ImageRepository implements ImageRepositoryInterface
{

    /**
     * @param array $attributes
     * @param User $user
     * @return Image
     */
    public function create(array $attributes, User $user): Image
    {
        $image = new Image();
        $image->fill([
            ImageInterfaces::ATTRIBUTE_USER_ID => $user->id,
            ImageInterfaces::ATTRIBUTE_STATUS => ImageInterfaces::STATUS_PROCESSING,
            ImageInterfaces::ATTRIBUTE_SETTINGS => Arr::get($attributes, 'settings'),
        ]);
        $image->save();

        return $image;
    }

    /**
     * @param Image $image
     * @param array $attributes
     * @return Image
     */
    public function update(Image $image, array $attributes): Image
    {
        $image->fill([
            ImageInterfaces::ATTRIBUTE_STATUS => Arr::get(
                $attributes,
                'status',
                ImageInterfaces::STATUS_PROCESSING
            ),
            ImageInterfaces::ATTRIBUTE_SETTINGS => Arr::get($attributes, 'settings'),
        ]);
        $image->save();

        return $image;
    }

    /**
     * @param User $user
     * @return Builder
     */
    public function list(User $user): Builder
    {
        return Image::query()
            ->where(ImageInterfaces::ATTRIBUTE_USER_ID, $user->id);
    }

}
