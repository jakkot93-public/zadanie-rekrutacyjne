<?php

namespace App\Domains\Image\Repository;

use App\Domains\Image\Models\Eloquent\Image;
use App\Models\Eloquent\User;
use Illuminate\Database\Eloquent\Builder;

interface ImageRepositoryInterface
{

    /**
     * @param array $attributes
     * @param User $user
     * @return Image
     */
    public function create(array $attributes, User $user): Image;

    /**
     * @param Image $image
     * @param array $attributes
     * @return Image
     */
    public function update(Image $image, array $attributes): Image;

    /**
     * @param User $user
     * @return Builder
     */
    public function list(User $user): Builder;
}
