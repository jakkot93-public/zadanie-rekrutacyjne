<?php

namespace App\Domains\Image\Jobs;

use App\Domains\Image\Models\Eloquent\Image;
use App\Domains\Image\Models\ImageInterfaces;
use App\Models\Eloquent\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ProcessImageScalingJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(
        public User  $user,
        public Image $image
    )
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $settings = $this->image->getSettings();

        // tutaj powinno byc procesowanie zdjecia
        $rawImagePath = Arr::get($settings, ImageInterfaces::SETTINGS_FILE_RAW);
        $processedImagePath = 'images/' . $this->user->uuid . '/processed/' . File::basename($rawImagePath);
        Storage::copy($rawImagePath, $processedImagePath);

        Arr::set($settings, ImageInterfaces::SETTINGS_FILE_PROCESSED, $processedImagePath);

        $this->image->update([
            ImageInterfaces::ATTRIBUTE_STATUS => ImageInterfaces::STATUS_FINISHED,
            ImageInterfaces::ATTRIBUTE_SETTINGS => $settings
        ]);

        // tutaj po skonczonym procesowaniu mogloby wyjsc powiadomienie do usera
    }
}
