<?php

namespace App\Domains\Image\Policies;

use App\Domains\Image\Models\Eloquent\Image;
use Illuminate\Contracts\Auth\Authenticatable;

class ImagePolicy
{

    /**
     * @param Authenticatable $user
     * @param Image $image
     * @return bool
     */
    public function view(Authenticatable $user, Image $image): bool
    {
        if ($user->id === $image->user_id) {
            return true;
        }

        return false;
    }

}
