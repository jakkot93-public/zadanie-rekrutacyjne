<?php

namespace App\Domains\Image\Services;

use App\Domains\Image\Http\Requests\ImageStoreRequest;
use App\Domains\Image\Jobs\ProcessImageConvertingJob;
use App\Domains\Image\Jobs\ProcessImageScalingJob;
use App\Domains\Image\Models\Eloquent\Image;
use App\Domains\Image\Models\ImageInterfaces;
use App\Domains\Image\Repository\Eloquent\ImageRepository;
use App\Models\Eloquent\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

class ImageService
{

    /**
     * @param ImageRepository $imageRepository
     */
    public function __construct(
        private ImageRepository $imageRepository
    )
    {
        //
    }

    /**
     * @param ImageStoreRequest $request
     * @param User $user
     * @return Image
     */
    public function create(ImageStoreRequest $request, User $user): Image
    {
        $file = $request->file('image');
        $path = $file->store('images/' . $request->user()->uuid . '/raw');

        $attributes = $request->validated();
        Arr::set($attributes, 'settings.raw_file', $path);
        Arr::set($attributes, 'settings.original_name', $file->getClientOriginalName());
        Arr::set($attributes, 'settings.original_extension', $file->getClientOriginalExtension());

        $image = $this->imageRepository->create($attributes, $user);

        $type = Arr::get($attributes, 'settings.type');
        if ($type === ImageInterfaces::SETTINGS_TYPE_SCALING) {
            ProcessImageScalingJob::dispatch($user, $image)->onQueue('images');
        } else {
            ProcessImageConvertingJob::dispatch($user, $image)->onQueue('images');
        }

        return $image;
    }

    /**
     * @param Image $image
     * @param array $attributes
     * @return Image
     */
    public function update(Image $image, array $attributes): Image
    {
        return $this->imageRepository->update($image, $attributes);
    }

    /**
     * @param User $user
     * @return Builder
     */
    public function list(User $user): Builder
    {
        return $this->imageRepository->list($user);
    }
}
