<?php

namespace App\Domains\Image\Models;

/**
 * Interface Image
 */
interface ImageInterfaces
{
    const TABLE_NAME = 'images';

    const ATTRIBUTE_ID = 'id';
    const ATTRIBUTE_USER_ID = 'user_id';
    const ATTRIBUTE_SETTINGS = 'settings';
    const ATTRIBUTE_STATUS = 'status';

    const STATUS_PROCESSING = 'processing';
    const STATUS_FINISHED = 'finished';
    const STATUS_FAILED = 'failed';

    const SETTINGS_TYPE_SCALING = 'scaling';
    const SETTINGS_TYPE_CONVERTING = 'converting';

    const SETTINGS_FILE_RAW = 'raw_file';
    const SETTINGS_FILE_PROCESSED = 'processed_file';

    const SETTINGS_TYPES = [
        self::SETTINGS_TYPE_SCALING,
        self::SETTINGS_TYPE_CONVERTING
    ];

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self;

    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId(int $userId): self;

    /**
     * @return int
     */
    public function getUserId(): int;

    /**
     * @param array $settings
     * @return $this
     */
    public function setSettings(array $settings): self;

    /**
     * @return array
     */
    public function getSettings(): array;

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus(string $status): self;

    /**
     * @return string
     */
    public function getStatus(): string;

}
