<?php

namespace App\Domains\Image\Models\Eloquent;

use App\Domains\Image\Models\ImageInterfaces;
use App\Models\Eloquent\User;
use App\Models\UserInterfaces;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model implements ImageInterfaces
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = ImageInterfaces::TABLE_NAME;

    /**
     * @var string
     */
    protected $primaryKey = self::ATTRIBUTE_ID;

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        ImageInterfaces::ATTRIBUTE_SETTINGS => 'array',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        ImageInterfaces::ATTRIBUTE_USER_ID,
        ImageInterfaces::ATTRIBUTE_SETTINGS,
        ImageInterfaces::ATTRIBUTE_STATUS,
    ];

    /**
     * @inheritDoc
     */
    public function setId(int $id): self
    {
        return $this->setAttribute(ImageInterfaces::ATTRIBUTE_ID, $id);
    }

    /**
     * @inheritDoc
     */
    public function getId(): int
    {
        return $this->getAttribute(ImageInterfaces::ATTRIBUTE_ID);
    }

    /**
     * @inheritDoc
     */
    public function setUserId(int $userId): self
    {
        return $this->setAttribute(ImageInterfaces::ATTRIBUTE_USER_ID, $userId);
    }

    /**
     * @inheritDoc
     */
    public function getUserId(): int
    {
        return $this->getAttribute(ImageInterfaces::ATTRIBUTE_USER_ID);
    }

    /**
     * @inheritDoc
     */
    public function setSettings(array $settings): self
    {
        return $this->setAttribute(ImageInterfaces::ATTRIBUTE_SETTINGS, $settings);
    }

    /**
     * @inheritDoc
     */
    public function getSettings(): array
    {
        return $this->getAttribute(ImageInterfaces::ATTRIBUTE_SETTINGS);
    }

    /**
     * @inheritDoc
     */
    public function setStatus(string $status): self
    {
        return $this->setAttribute(ImageInterfaces::ATTRIBUTE_STATUS, $status);
    }

    /**
     * @inheritDoc
     */
    public function getStatus(): string
    {
        return $this->getAttribute(ImageInterfaces::ATTRIBUTE_STATUS);
    }

    /**
     * @return HasOne
     */
    public function user(): HasOne
    {
        return $this->hasOne(
            User::class,
            UserInterfaces::ATTRIBUTE_ID,
            self::ATTRIBUTE_USER_ID);
    }
}
