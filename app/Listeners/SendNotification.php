<?php

namespace App\Listeners;

use App\Events\UserCreating;
use App\Mail\RegisterMail;
use Illuminate\Support\Facades\Mail;

class SendNotification
{
    /**
     * @var RegisterMail
     */
    private $registerMail;

    public function __construct(RegisterMail $registerMail)
    {
        $this->registerMail = $registerMail;
    }

    public function handle(UserCreating $event)
    {
        $this->registerMail->setUser($event->user);
        Mail::to($event->user->getEmail())->send($this->registerMail);
    }
}
