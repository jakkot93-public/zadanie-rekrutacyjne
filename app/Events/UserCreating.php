<?php

namespace App\Events;

use App\Models\Eloquent\User;

class UserCreating
{

    /**
     * @var User
     */
    public $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

}
