<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\AbstractApiController;
use App\Http\Requests\Auth\AuthRegisterRequest;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\Eloquent\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class AuthController extends AbstractApiController
{

    /**
     * @OA\Post(
     *     path="/api/auth/register",
     *     operationId="register",
     *     tags={"Auth"},
     *     summary="User Register",
     *     description="User register",
     *     @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/AuthRegisterRequest")
     *      ),
     *     @OA\Response(
     *         response=201,
     *         description="Register Successfully",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 format="string",
     *                 default="6|ZHyfiWvjwWx29vF8WAU64pWKzE9xneb5C0r8PbVm",
     *                 property="access_token"
     *             ),
     *             @OA\Property(
     *                format="string",
     *                 default="Bearer",
     *                 property="token_type"
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad request",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 property="error",
     *                 @OA\Property(
     *                     property="email",
     *                     default="The email must be a valid email address.",
     *                 )
     *             )
     *         )
     *     ),
     * )
     *
     * @param AuthRegisterRequest $request
     * @return JsonResponse
     */
    public function register(AuthRegisterRequest $request): JsonResponse
    {
        $user = User::create([
            'name' => $request->get('login'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password'))
        ]);
        $token = $user->createToken('auth_token')->plainTextToken;

        return $this->response(
            [
                'token_type' => 'Bearer',
                'access_token' => $token,
            ],
            ResponseAlias::HTTP_CREATED
        );
    }

    /**
     * @OA\Post(
     *     path="/api/auth/login",
     *     operationId="login",
     *     tags={"Auth"},
     *     summary="Login",
     *     description="Login",
     *     @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/LoginRequest")
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Getting access token",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(
     *                 format="string",
     *                 default="6|ZHyfiWvjwWx29vF8WAU64pWKzE9xneb5C0r8PbVm",
     *                 property="access_token"
     *             ),
     *             @OA\Property(
     *                 format="string",
     *                 default="Bearer",
     *                 property="token_type"
     *             ),
     *         )
     *     ),
     *     @OA\Response(response=401, description="Invalid login details"),
     * )
     *
     *
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request): JsonResponse
    {
        if (!Auth::attempt($request->only('email', 'password'))) {
            return $this->response(
                [
                    'error' => __('api.error_message.invalid_login')
                ],
                ResponseAlias::HTTP_UNAUTHORIZED
            );
        }
        $user = User::where('email', $request['email'])->first();
        $user->tokens()->delete();

        return $this->response(
            [
                'token_type' => 'Bearer',
                'access_token' => $user->createToken('auth_token')->plainTextToken,
            ]
        );
    }
}
