<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

/**
 * @OA\Info(
 *     title="Zadanie rekrutacyjne",
 *     version="1.0.0",
 *     @OA\Contact(
 *         email="jakkot93@gmail.com"
 *     )
 * )
 * @OA\Server(
 *     description="Localhost API",
 *     url="http://localhost"
 * )
 *
 * Class AbstractApiController
 */
abstract class AbstractApiController extends Controller
{

    protected const PER_PAGE_DEFAULT = 5;

    /**
     * Standard JSON reponse.
     *
     * @param array $data
     * @param int $httpCode
     *
     * @return JsonResponse
     */
    protected function response(array $data = [], int $httpCode = Response::HTTP_OK): JsonResponse
    {
        return response()->json(
            $data,
            $httpCode
        );
    }
}
